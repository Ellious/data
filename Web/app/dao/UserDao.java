package dao;

import com.google.inject.Inject;
import models.User;
import play.Logger;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import javax.persistence.NoResultException;
import java.util.Date;

/**
 * Created by ellious on 05.10.16.
 */
@Transactional
public class UserDao {

    private static final Logger.ALogger LOGGER = Logger.of(UserDao.class);
    @Inject
    JPAApi jpaApi;

    private User find(Long id) throws NoResultException {
        return jpaApi.em()
                .createNamedQuery("User.getById", User.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public User get(Long id) {
        try{
            return find(id);
        } catch (NoResultException noex){
            LOGGER.error(noex.getMessage(), noex);
            return null;
        }
    }

    private User find (String email) {
        LOGGER.debug("find user");
        return jpaApi.em()
                .createNamedQuery("User.getByEmail", User.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    public User get (String email) {
        try {
            LOGGER.debug("get user: " + email);
            return find(email);
        } catch (NoResultException ex) {
            LOGGER.debug("error find");
            LOGGER.error(ex.getMessage(), ex);
            return null;
        } catch (Exception ex){
            LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    private void save(User user){
        jpaApi.em().persist(user);
    }

    public User create(String email, String firstName, String lastName,
                           Date birthday, String gender){
        User user = new User();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setBirthday(birthday);
        user.setGender(gender);
        user.setDtCreated(new Date());
        user.setDeleted(false);
        save(user);

        return get(email);
    }

}
