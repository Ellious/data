package dao;

import com.google.inject.Inject;
import models.Security;
import models.User;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.persistence.NoResultException;

/**
 * Created by ellious on 05.10.16.
 */
public class SecurityDao {

    private static final Logger.ALogger LOGGER = Logger.of(SecurityDao.class);
    @Inject
    JPAApi jpaApi;

    private void save (Security security) {
        jpaApi.em().persist(security);
    }

    public void create (User user, String password) {
        save(new Security(user, password));
    }

    private Security find(Long userId) throws NoResultException {
        return jpaApi.em()
                .createNamedQuery("Security.getByUserId", Security.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    public Security get(Long userId) {
        try{
            return find(userId);
        }catch (NoResultException noex){
            LOGGER.error(noex.getMessage(), noex);
            return null;
        }
    }



    

}
