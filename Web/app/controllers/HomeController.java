package controllers;

import com.google.inject.Inject;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    @Inject
    FormFactory formFactory;

    public Result index() {
        return ok(index.render(null));
    }


    public Result test(String name, int age) {
        return ok(test_vw.render(name, age));
    }

    public Result task2(String color, String figure)  {
        return ok(task2_vw.render(color, figure));
    }

    public Result task3(){
        try{
            Form form = formFactory.form().bindFromRequest();
            Double number1 = Double.parseDouble((String) form.data().get("number_1"));
            Double number2 = Double.parseDouble((String) form.data().get("number_2"));
            String action = (String) form.data().get("action");

            Double result;

            switch (action){
                case "+":
                    result = number1 + number2;
                    break;
                case "-":
                    result = number1 - number2;
                    break;
                case "*":
                    result = number1 * number2;
                    break;
                case "/":
                    result = number1 / number2;
                    break;
                default:
                    result = 0.0;
            }

            return ok(task3_vw.render(number1, number2, action, result));

        }catch (Exception ex){
            return badRequest(index.render(ex.getMessage()));
        }
    }
    public Result task4() {
        Form form = formFactory.form().bindFromRequest();
        String animalName= (String) form.data().get("animal_name");
        Double animalSize = Double.parseDouble((String) form.data().get("animal_size"));
        String animalColor= (String) form.data().get("color");
        return ok(task4_vw.render(animalName, animalSize, animalColor));
    }

}
