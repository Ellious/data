package controllers;

import com.google.inject.Inject;
import forms.ColorForm;
import forms.Task1Form;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.task1.task1_1vw;
import views.html.task1.task1_2_formvw;
import views.html.task1.task1_vw;
import views.html.task1.task_color_input_vw;
import views.html.task1.task_color_output_vw;

/**
 * Created by ellious on 24.10.16.
 */


public class Task1Controller extends Controller {

    public Result message() {
        return ok( task1_vw.render() );
    }

    @Inject
    FormFactory formFactory;


    // Sending POST

    public Result sendPost() {
        Form<Task1Form> form = formFactory.form(Task1Form.class).bindFromRequest();
        return ok(task1_2_formvw.render(form));
    }


    // Sending GET
    public Result sendGet(String firstName, String lastName) {
        return ok(task1_1vw.render(firstName, lastName));
    }

    public Result inputColorForm() {
        return ok (task_color_input_vw.render() ) ;
    }

    public Result outputColorByForm() {
        Form <ColorForm> form = formFactory.form(ColorForm.class).bindFromRequest();
        return ok (task_color_output_vw.render(form));
    }



 }
