package controllers;

/**
 * Created by ellious on 19.10.16.
 */

import com.google.inject.Inject;
import dao.SecurityDao;
import dao.UserDao;
import forms.LoginForm;
import models.Security;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login_form;
import views.html.welcome;

public class LoginController extends Controller{

    @Inject
    FormFactory formFactory ;

    @Inject
    private UserDao userDao;
    @Inject
    private SecurityDao securityDao;

    public Result login() {
            return ok(login_form.render(formFactory.form(LoginForm.class)));
    }

    @Transactional(readOnly = true)
    public Result authorise(){

        Form<LoginForm> form = formFactory.form(LoginForm.class).bindFromRequest();

        if(form.hasErrors()){
            return badRequest(login_form.render(form));
        }

        try{
            LoginForm loginForm = form.get();
            String email = loginForm.getEmail();
            String password = loginForm.getPassword();

            User user = userDao.get(email);
            if(user == null){
                form.reject("email", "Email not found");
                return badRequest(login_form.render(form));
            }

            Security security = securityDao.get(user.getId());

            if(!security.check(password)){
                form.reject("password", "Wrong password or email");
                return badRequest(login_form.render(form));
            }

            return redirect(routes.LoginController.welcome(user.getId()));
        } catch (Exception ex){
            form.reject("System error");
            return badRequest(login_form.render(form));
        }
    }

    @Transactional(readOnly = true)
    public Result welcome(Long clientId){
        return ok(welcome.render(userDao.get(clientId)));
    }

}