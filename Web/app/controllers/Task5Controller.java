package controllers;

import com.google.inject.Inject;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.task4_vw;

/**
 * Created by ellious on 03.09.16.
 */


public class Task5Controller extends  Controller {

    @Inject
    FormFactory formFactory;

    public Result index() {
        return ok(index.render(null));
    }


    public Result   task5() {
        Form form = formFactory.form().bindFromRequest();
        String animalName= (String) form.data().get("animal_name");
        Double animalSize = Double.parseDouble((String) form.data().get("animal_size"));
        String animalColor= (String) form.data().get("color");
        return ok(task4_vw.render(animalName, animalSize, animalColor));
    }

}