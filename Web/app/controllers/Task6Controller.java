package controllers;

import com.google.inject.Inject;
import forms.LoginForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.task6.authed_vw;
import views.html.task6.login_vw;

/**
 * Created by ellious on 03.09.16.
 */
public class Task6Controller extends Controller {


    @Inject
    FormFactory formFactory ;
    private static final String LOGIN = "panda";
    private static final String PASSWORD = "ilovebamboo";

    public Result task6() {
        return ok(login_vw.render(formFactory.form(LoginForm.class)));
    }

    public Result login(){

        // creating New typed form as LoginForm class for Login
        Form<LoginForm> form = formFactory.form(LoginForm.class).bindFromRequest();

        if(form.hasErrors()){
            return badRequest(login_vw.render(form));
        }

        try{

            LoginForm loginForm = form.get();

            String login = loginForm.getEmail();
            String password = loginForm.getPassword();

            if(LOGIN.equals(login) && PASSWORD.equals(password)){
                return ok(authed_vw.render(login));
            } else {
                form.reject("Bad password or wrong login");
                return badRequest(login_vw.render(form));
            }
        } catch (Exception ex){
            return badRequest(login_vw.render(form));
        }
    }
}
