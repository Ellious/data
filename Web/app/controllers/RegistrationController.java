package controllers;

import com.google.inject.Inject;
import dao.SecurityDao;
import dao.UserDao;
import forms.RegistrationForm;
import models.User;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.registration.registration_vw;
import views.html.registration.success_registration_vw;

import java.util.Date;


/**
 * Created by ellious on 19.09.16.
 */


public class RegistrationController extends Controller {

    private static final Logger.ALogger LOGGER = Logger.of(RegistrationController.class);
    @Inject
    private FormFactory formFactory;
    @Inject
    private UserDao userDao;
    @Inject
    private SecurityDao securityDao;

    public Result registration() {
        return ok(registration_vw.render(formFactory.form(RegistrationForm.class)));
    }

    @Transactional
    public Result sendRegistration() {

        Form<RegistrationForm> form = formFactory.form(RegistrationForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(registration_vw.render(form));
        }

        try {
            RegistrationForm registrationForm = form.get();
            String firstName = registrationForm.getFirstName();
            String lastName = registrationForm.getLastName();
            Date birthday = registrationForm.getBirthday();
            String gender = registrationForm.getGender();
            String email = registrationForm.getEmail();
            String password = registrationForm.getPassword();
            String confirmPassword = registrationForm.getConfirmPassword();

            LOGGER.debug("1");

            if(!password.equals(confirmPassword)) {
                form.reject("confirmPassword", "passwords are not same");
                return badRequest(registration_vw.render(form));
            }
            LOGGER.debug("2");

            if(userDao.get(email) != null){
                LOGGER.debug("2.1");
                form.reject("email", "Email already used");
                return badRequest(registration_vw.render(form));
            }
            LOGGER.debug("3");

            User user = userDao.create(email, firstName, lastName, birthday, gender);
            securityDao.create(user, password);
            LOGGER.debug("4");

            return ok(success_registration_vw.render(user));
        }
        catch (Exception ex) {
            return badRequest(registration_vw.render(form));
        }
    }


}
