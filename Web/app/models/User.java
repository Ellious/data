package models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ellious on 05.10.16.
 */
@Entity
@Table(name = "user")
@NamedQueries({
        @NamedQuery(name = "User.getById", query = "SELECT u FROM User u WHERE u.id = :id"),
        @NamedQuery(name = "User.getByEmail", query = "SELECT u FROM User u WHERE u.email = :email")
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private Date birthday;
    private String gender;
    @Column(name = "dt_created")
    private Date dtCreated;
    private Boolean deleted;
    @Column(name = "dt_deleted")
    private Date dtDeleted;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getDtDeleted() {
        return dtDeleted;
    }

    public void setDtDeleted(Date dtDeleted) {
        this.dtDeleted = dtDeleted;
    }

}
