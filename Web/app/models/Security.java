package models;

import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;

/**
 * Created by ellious on 05.10.16.
 */
@Entity
@Table(name = "security")
@NamedQuery(name = "Security.getByUserId",  query = "SELECT s FROM Security s WHERE s.user.id = :userId")
public class Security {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
    private String password;

    public Security(){

    }

    public Security(User user, String password) {
        this.user = user;
        this.password = hash(password);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = hash(password);
    }

    private String hash (String password)  {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public Boolean check(String password){
        return BCrypt.checkpw(password, this.password);
    }
}
