package forms;

import play.data.format.Formats;
import play.data.validation.Constraints;

import java.util.Date;

/**
 * Created by ellious on 19.09.16.
 */
public class RegistrationForm {

    @Constraints.Required
    @Constraints.Pattern(value="[A-Za-z]*")
    private String firstName;

    @Constraints.Required
    @Constraints.Pattern(value="[A-Za-z]*")
    private String lastName;

    @Constraints.Required
    @Formats.DateTime(pattern="dd.MM.yyyy")
    private Date birthday;

    @Constraints.Required
    private String gender;

    @Constraints.Required
    @Constraints.Email()
    private String email;

    @Constraints.Required
    @Constraints.MinLength(8)
    @Constraints.MaxLength(24)
//    @Constraints.Pattern(value="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{0,}$")
    private String password;

    @Constraints.Required
    private String confirmPassword;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password != null ? password : "";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword != null ? confirmPassword : "";
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
