package forms;

import play.data.validation.Constraints;

/**
 * Created by ellious on 07.11.16.
 */
public class ColorForm {
    @Constraints.Required
    private String color;

    @Constraints.Required
    private String shade;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getShade() {
        return shade;
    }

    public void setShade(String shade) {
        this.shade = shade;
    }


}
