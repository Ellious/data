package forms;

import play.data.validation.Constraints;

/**
 * Created by ellious on 26.10.16.
 */
public class Task1Form {

    @Constraints.Required
    private String firstName;

    @Constraints.Required
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



}
