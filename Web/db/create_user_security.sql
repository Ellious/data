CREATE TABLE `play_db`.`user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(70) NOT NULL,
  `firstname` VARCHAR(50) NOT NULL,
  `lastname` VARCHAR(50) NOT NULL,
  `birthday` DATE NOT NULL,
  `gender` VARCHAR(10) NOT NULL,
  `dt_created` TIMESTAMP NOT NULL,
  `deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `dt_deleted` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
PACK_KEYS = DEFAULT, ENGINE=INNODB;


CREATE TABLE `play_db`.`security` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT NOT NULL,
  `password` VARCHAR(450) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_security_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `play_db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) ENGINE=INNODB;